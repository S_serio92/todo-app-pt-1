import React, { Component } from 'react';

class GetId extends Component {
    state = {
        users: [{ id: 1 }, { id: 2 }, { id: 6 }]
    }
    handleDelete = (event, usersId) => {
    const newUsers = this.state.users.filter(
        users => users.id !== usersId
    )
    this.setState({ users: newUsers })
}
render() {
    return (
        <React.Fragment>
            <h1>Active User</h1>
            {this.state.users.map(users => (
                <div>
                    <p> Users: {users.id}</p>
                    <button onClick={event => this.handleDelete(event, users.id)}>
                        Delete Users
        </button>
                </div>
            ))}
        </React.Fragment>
    )
}
 }

export default GetId;